import { Component } from '@angular/core';
import { User, Database } from '@ionic/cloud-angular';
import { NavController, ModalController } from 'ionic-angular';
import { BabyPage } from '../baby/baby';
import { DiaperPage } from '../diaper/diaper';
import { FeedingPage } from '../feeding/feeding';

@Component({
  selector: 'page-events',
  templateUrl: 'events.html'
})
export class EventsPage {
  public events: any = [];
  private subscription: any;
  private listDuration: String = 'Today';
  private totalEvents: number = 1;
  /**
   * Default Construtor
   */
  constructor(public modalCtrl: ModalController,
    public navCtrl: NavController,
    public user: User,
    public db: Database) {
  }

  /**
   * Occurs when the view becomes active.
   */
  private ionViewDidEnter(): void {
    let beginningOfToday = new Date();
    beginningOfToday.setHours(0, 0, 0, 0);
    // Connect to the database.
    this.db.connect();
    this.subscription = this.db.collection("event").order("millisecondTimestamp", "descending").findAll({ owner: this.user.id }).limit(this.totalEvents * 25).watch().subscribe((evts) => {
      this.events = evts;
    });
  }

  /**
   * Occurs when the view becomes unactive.
   */
  private ionViewWillLeave(): void {
    this.subscription.unsubscribe();
  }

  /**
   * Occurs when the delete button is clicked on one of the list items.
   * @param id
   */
  private onDeleteBtnClicked(id: String): void {
    this.db.collection("event").remove({ id: id.toString() });
  }

  /**
   * Occurs when the feeding button is clicked.
   */
  private onFeedingBtnClicked(): void {
    let popover = this.modalCtrl.create(FeedingPage);
    popover.present();
  }

  /**
   * Occurs when the diaper button is clicked.
   */
  private onDiaperBtnClicked(): void {
    let popover = this.modalCtrl.create(DiaperPage);
    popover.present();
  }

  /**
   * Occurs when the baby button is clicked.
   */
  private onBabyBtnClicked(): void {
    let popover = this.modalCtrl.create(BabyPage);
    popover.present();
  }

  /**
   * Occurs when the list performs the infinite scroll operation.
   * @param event
   */
  private doInfinite(infiniteScroll: any): void {
    setTimeout(() => {
      this.totalEvents += 1;
      infiniteScroll.complete();
    }, 500);
  }

}
