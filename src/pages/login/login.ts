import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { FacebookAuth, User, Auth } from '@ionic/cloud-angular';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  constructor(public viewCtrl: ViewController,
    public facebookAuth: FacebookAuth,
    public user: User,
    public auth: Auth) {
  }

  /**
   * occurs when the facebook button is pressed.
   */
  private facebookLoginPressed(): void {
    this.facebookAuth.login().then(() => {
      this.viewCtrl.dismiss();
    });
  }

  /**
   * occurs when the facebook button is pressed.
   */
  private twitterLoginPressed(): void {
    this.auth.login('twitter').then(() => {
      this.viewCtrl.dismiss();
    });
  }
}
