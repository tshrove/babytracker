import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { FacebookAuth, User, Auth, Database } from '@ionic/cloud-angular';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  public children: any;
  private subscription: any;

  /** 
   * Default Constructor
   */
  constructor(public modalCtrl: ModalController,
    public facebookAuth: FacebookAuth,
    public user: User,
    public db: Database,
    public auth: Auth) {
  }
  /**
   * Occurs when the view becomes active.
   */
  private ionViewDidEnter(): void {
    // load the user.
    this.user.load().then(() => {
      // get the children associated with the user.
      this.db.collection('baby').findAll({ parent: this.user.id }).fetch().subscribe((babies) => {
        this.children = babies;
      });
    });
  }

  /**
   * Occurs when the facebook auth is logged out.
   */
  public onLogoutBtnClicked(): void {
    // if facebook logged in, sign out.
    if (this.user.social.facebook != null) {
      this.facebookAuth.logout().then(() => {
        this.presentLoginPage();
      });
      // if twitter logged in, sign ou.
    } else if (this.user.social.twitter != null) {
      this.auth.logout();
      this.presentLoginPage();
    }
  }

  /**
   * Present the login screen if not authenticated.
   */
  private presentLoginPage(): void {
    if (!this.auth.isAuthenticated()) {
      let popover = this.modalCtrl.create(LoginPage);
      popover.present();
    }
  }
}
