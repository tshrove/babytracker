import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { User, Database, Auth } from '@ionic/cloud-angular';

@Component({
  selector: 'page-diaper',
  templateUrl: 'diaper.html'
})
export class DiaperPage {
  private diaperType: String = "dirty";
  private dateOfOccurence: String = new Date().toISOString();
  private children: any;
  private who: String;
  private maxDate: String;

  /**
   * Default Constructor
   */
  constructor(public viewCtrl: ViewController,
    public navCtrl: NavController,
    public user: User,
    public db: Database) {

    // set the max date on the datetime picker.
    let myDate = new Date();
    this.maxDate = myDate.toISOString().substring(0, 10);

    myDate.setHours(myDate.getHours() - (myDate.getTimezoneOffset() / 60));
    this.dateOfOccurence = myDate.toISOString();
  }

  private ionViewDidEnter(): void {
    // Get all the current user's children from the database.
    this.db.collection('baby').findAll({ parent: this.user.id }).fetch().subscribe((babies) => {
      this.children = babies;
      if (babies.length > 0 && babies.length == 1) {
        this.who = babies[0].id;
      }
    });
  }

  /**
   * Occurs when the close button is clicked.
   */
  private onCloseBtnClicked(): void {
    this.viewCtrl.dismiss();
  }
  /**
   * Occurs when the save button is clicked.
   */
  private onSaveBtnClicked(): void {
    // find the baby with the specific id.
    this.db.collection('baby').find({ id: this.who }).fetch().subscribe((baby) => {
      let date = new Date(Date.parse(this.dateOfOccurence.toString()));
      date.setHours(date.getHours() + date.getTimezoneOffset() / 60);
      // add the event to the database.
      this.db.collection('event').store({
        "type": "diaper",
        "baby": baby,
        "value": this.diaperType,
        "timestamp": date,
        "millisecondTimestamp": date.getTime(),
        "owner": this.user.id
      });
      // dismiss the view.
      this.viewCtrl.dismiss();
    });
  }
}
