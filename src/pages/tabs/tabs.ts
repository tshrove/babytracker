import { Component } from '@angular/core';
import { ProfilePage } from '../profile/profile';
import { EventsPage } from '../events/events';
import { LoginPage } from '../login/login';
import { DashboardPage } from '../dashboard/dashboard';
import { ModalController } from 'ionic-angular';
import { User, Auth } from '@ionic/cloud-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab0Root: any = DashboardPage;
  tab1Root: any = EventsPage;
  tab2Root: any = ProfilePage;

  /**
   * Default Constructor
   */
  constructor(public modalCtrl: ModalController,
    public user: User,
    public auth: Auth) {
    this.presentLoginPage();
  }

  /**
   * Present the login screen if not authenticated.
   */
  private presentLoginPage(): void {
    if (!this.auth.isAuthenticated()) {
      let popover = this.modalCtrl.create(LoginPage);
      popover.present();
    }
  }
}
