import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { User, Database, Auth } from '@ionic/cloud-angular';

@Component({
  selector: 'page-baby',
  templateUrl: 'baby.html'
})
export class BabyPage {
  public gender: String = "boy";
  public birthdate: Date;
  public fullname: String;
  /**
   * Default Constructor
   */
  constructor(public viewCtrl: ViewController,
    public user: User,
    public navCtrl: NavController,
    public db: Database) {
  }
  /**
   * Occurs when the close button is clicked.
   */
  private onCloseBtnClicked(): void {
    this.viewCtrl.dismiss();
  }
  /**
   * Occurs when the save button is clicked.
   */
  private onSaveBtnClicked(): void {
    this.db.collection('baby').store({
      "fullname": this.fullname,
      "birthdate": this.birthdate,
      "gender": this.gender,
      "parent": this.user.id
    });
    this.viewCtrl.dismiss();
  }
}
