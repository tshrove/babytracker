import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { EventsPage } from '../pages/events/events';
import { BabyPage } from '../pages/baby/baby';
import { DiaperPage } from '../pages/diaper/diaper';
import { FeedingPage } from '../pages/feeding/feeding';
import { TabsPage } from '../pages/tabs/tabs';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { MomentModule } from 'angular2-moment';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': 'e7711c70'
  },
  'auth': {
    'facebook': {
      'scope': ['email', 'public_profile']
    }
  }
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    EventsPage,
    TabsPage,
    LoginPage,
    ProfilePage,
    BabyPage,
    FeedingPage,
    DiaperPage,
    DashboardPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    CloudModule.forRoot(cloudSettings),
    MomentModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    EventsPage,
    TabsPage,
    LoginPage,
    ProfilePage,
    BabyPage,
    FeedingPage,
    DiaperPage,
    DashboardPage
  ],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler }]
})
export class AppModule { }
